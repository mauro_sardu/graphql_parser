# GraphQL_parser from MongoDb (Based on Nick Redmark's example https://github.com/nmaro/graphql-mongodb-example)
# CMS RAS adaptation by Mauro Sardu for SardegnaIT

All the important code is in `src/start.js`.

Install, build and run:

```
yarn install
yarn run build
yarn start
```

For Local Development 

You need to start Mongodb for Local development 

```
npm run startdev
```