import {MongoClient, ObjectId} from 'mongodb'
import express from 'express'
import bodyParser from 'body-parser'
import {graphqlExpress, graphiqlExpress} from 'graphql-server-express'
import {makeExecutableSchema} from 'graphql-tools'
import cors from 'cors'
import {prepare} from "../util/index"


const app = express()

app.use(cors())


export const start = async () => {
  try {
    const db = await MongoClient.connect(process.env.MONGO_URL)

    const Contents = db.collection(process.env.MONGO_COLLECTION)
    
    const typeDefs = [`
      type Query {
        getContent(_id: ID): Post
        getContents(match: GetContentsInput, filter: filterInput, orderBy: ContentOrderByInput, recordsForPage: Int, pageNumber: Int): [Post]        
        getImage(_id: String): Image
        getImages: [Image]        
        getDocument(_id: String): Document
        getDocuments: [Document]
        getLinklist(_id: String): linkList
        getlinkLists: [linkList]
        getVideoLists: [Video]
        getContentsByType(tipoContenuto: String): [Post]
      }

      type Post {
        _id: ID
        tipoContenuto: String
        idContenuto: String
        titolo: String
        breve: String
        corpo: String
        stato: String
        dataCreazione: String                
        dataModifica: String
        dataPubblicazione: String
        immaginiList: [Image]
        documentiList: [Document]
        linkList: [linkList]
        videoList: [Video]
        categorieSorgente: String 
      }

      type Image {
          idImmagine: String
          urlSorgente: String
          alt: String
          breve: String
          tipoImmagine: String        
        }
      
      type Document{
            idDocumento: String
            titolo: String
            breve: String
            urlSorgente: String
            urlFinale: String
            tipoDocumento: String
         
      } 
      
      type Video{
        idVideo: String
        urlSorgente: String
      }
      
      type linkList
        {
          titolo: String
          url: String
          tipoVisualizzazione: String
          testo: String
          linkInternoRas: Boolean
       }

      
      schema {
        query: Query
      }

      input GetContentsInput {
        _id: ID
        id_notizia: Int
        tipoContenuto: String
        categorieSorgente: String
        last: Int
    }

    input filterInput {
      titolo: String
      breve: String
      corpo: String
      tags: [String]
      categorieSorgente: String
  }
      enum ContentOrderByInput{
        dataCreazione_ASC
        dataCreazione_DESC
        dataModifica_ASC
        dataModifica_DESC
      }
    

    `];


    

    const resolvers = {
      Query: {
        getContent: async (root, {_id}) => {
          return prepare(await Contents.findOne(ObjectId(_id)))
        },
        getContentsByType: async (root, {tipoContenuto}) => {
          return (await Contents.find({tipoContenuto: tipoContenuto}).toArray()).map(prepare)
        },
        getContents: async (parent, args, context, info) => {
          
          var recordsForPage=(args.recordsForPage)?args.recordsForPage:20;
          var pageNumber=(args.pageNumber)?args.pageNumber:1;
          var skip = recordsForPage*(pageNumber-1);
          if(args.orderBy){
            var orders = args.orderBy.split("_")
            var orderBy = {
              [orders[0]]: (orders[1]== "ASC")? 1:-1,
            } ;
            
          }
          var match = {};
          if(args.match){
            Object.keys(args.match).forEach(function(key) {
              match[key] = args.match[key]
            });
          }

          var filter = {};
          if(args.filter){
            Object.keys(args.filter).forEach(function(key) {
              filter['$regex'] = args.filter[key];
              filter['$options'] = "im"
              match[key] = filter;
            });
          }
          return (await Contents.aggregate([
             {$match: match},
             {$sort: orderBy }
          ]).skip(skip).limit(recordsForPage).toArray()).map(prepare)
        },
      },
    }

    const schema = makeExecutableSchema({
      typeDefs,
      resolvers
    })


    app.use('/graphql', bodyParser.json(), graphqlExpress({schema}))


    app.use(process.env.homePath, graphiqlExpress({
      endpointURL: process.env.homePath
    }))

    app.listen(process.env.PORT, () => {
      console.log(`Visit ${process.env.URL}:${process.env.PORT}${process.env.homePath}`)
    })

  } catch (e) {
    console.log(e)
  }

}
